﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(steer.Startup))]
namespace steer
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
