﻿using LinqToExcel;
using steer.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace steer.Controllers
{
    public class HomeController : Controller
    {
        public UpFile.ApplicationDbContext db = new UpFile.ApplicationDbContext();
        [Authorize]
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        [Authorize]
        public ActionResult Index(HttpPostedFileBase file)
        {
            if (Request.Files["file"].ContentLength > 0)
            {
                string fileExtension =
                                     System.IO.Path.GetExtension(Request.Files["file"].FileName);

                if (fileExtension == ".xls" || fileExtension == ".xlsx" || fileExtension == ".docx" || fileExtension == ".doc")
                {
                    string fileLocation = Server.MapPath("~/Uploaded/") + Request.Files["file"].FileName;
                    if (System.IO.File.Exists(fileLocation))
                    {

                        System.IO.File.Delete(fileLocation);
                    }
                    Request.Files["file"].SaveAs(fileLocation);
                }
                var purpose = Request.Form["purpose"];
                var up = new UpFile {
                    Name = Request.Files["file"].FileName,
                    Date = DateTime.Now,
                    Size = (Request.Files["file"].ContentLength + 1000) / 1000, // Bæti við 1000 því deilingin námundar niður. Deili með 1000 því mælieiningin á síðunni á að vera í kB.
                    FileType = fileExtension,
                    Purpose = purpose,
                };
                if (purpose == "devices")
                {
                    var excel = new ExcelQueryFactory(AppDomain.CurrentDomain.BaseDirectory + "Uploaded\\" + Request.Files["file"].FileName);
                    var firstSheet = excel.GetWorksheetNames().First();
                    var rows = from c in excel.Worksheet(firstSheet)
                               select c;

                    up.Columns = String.Join(";", excel.GetColumnNames(firstSheet));
                    foreach (var row in rows)
                    {
                        var joined = string.Join(";", row);
                        var device = new Device();
                        device.UpFile = up;
                        device.values = joined;
                        foreach (string cell in row)
                        {
                            if (cell.Contains("GRE-"))
                            {
                                device.gre = cell;
                            }
                        }
                        db.Devices.Add(device);
                    }   
                }
                db.UpFiles.Add(up);
                db.SaveChanges();
            }
            return View();
        }
    }
}