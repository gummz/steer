﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using steer.Models;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml;
using ClosedXML.Excel;
using System.Web;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Data.Entity.Validation;
using System.Globalization;

namespace steer.Controllers
{
    public class DocsController : Controller
    {
        private UpFile.ApplicationDbContext db = new UpFile.ApplicationDbContext();
        private const string outFolder = "Output\\";
        private const string uploaded = "Uploaded\\";
        private string root = AppDomain.CurrentDomain.BaseDirectory;
        private List<Dictionary<string, string>> dictList = new List<Dictionary<string, string>>();
        private Doc globalDoc = new Doc();

        // GET: Docs
        [Authorize]
        public ActionResult Index()
        {
            return View(db.Docs.ToList());
        }
        // Aðalfallið. Hér gerist galdurinn.
        [Authorize]
        public ActionResult Send(string rows)
        {
            string values;
            List<Device> devices;
            ProcessSendString(rows, out values, out devices);

            CompanyLicense companyLicense;
            UpFile devDownFile, up;
            List<string> excelFileNames;
            List<UpFile> outFilesToShow;
            Dictionary<string, string> dict;
            string fileName;
            UpFile wordFile;
            var valueDict = new Dictionary<string, Dictionary<string, string>>();
            StartExcel(rows, devices, out companyLicense, out devDownFile, out excelFileNames, out outFilesToShow, out dict, out fileName, out up, values, out wordFile, out valueDict);

            // End of Excel processing
            // ATHUGIÐ! /Output/ má ekki vera opið í Windows Explorer. Lokið því.
            // Annars kemur oft IOException. (að verið sé að nota skrána)

            // Begin Word processing
            StartWord(wordFile, companyLicense, devices, dict, valueDict, outFilesToShow);
            // End Word processing
            //db.SaveChanges();
            PrepareViewBag(devices, companyLicense, excelFileNames, wordFile);

            return PartialView("_SelectedRows", outFilesToShow);
        }

        // Word fallið, næsta stig fyrir neðan Send.
        private void StartWord(UpFile wordFile, CompanyLicense companyLicense, List<Device> devices, Dictionary<string, string>  dict, Dictionary<string, Dictionary<string, string>> valueDict, List<UpFile> outFilesToShow)
        {
            string src = InitializeWordObjects(wordFile, outFilesToShow);

            using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(src, true))
            {
                Regex regexText = new Regex("");
                string regex = "";
                var el0 = dictList.ElementAt(0);
                string docText = GetText(wordDoc);

                docText = WriteAuthorLicense(ref docText, ref regexText, el0); 
                docText = WriteDeviceList(ref regex, ref docText);
                docText = WriteValues(valueDict, ref regexText, ref docText);

                /*
                var i = 0;
                var occurred = "";
                foreach (var entry in valueDict)
                {
                    if (entry.Key == "c8" || entry.Key == "c9" || entry.Key == "c10" || entry.Key == "v5")
                    {
                        occurred += entry.Key + ":" + entry.Value + ";";
                        i++;
                    }
                }
                Mælarnir. ( {{insertMetres}} í Skýrsla.docx ) 
                Hugmyndin var þessi: Þar sem notandinn velur mæla fyrir hvert tæki þá þarf að draga valda mæla upp úr öllum tækjunum.
                Ég ímyndaði mér streng sem innihéldi þetta allt saman. Svo bara prófarðu t.d.
                if occurred contains c8: write c8 to doc
                if occurred contains c9: write c9 to doc
                o.s.frv.
                */
                WriteToDoc(wordDoc, docText);
            }
        }
        // Word fall - skoðaðu Skýrlsa.docx fyrir nánari upplýsingar
        private string WriteValues(Dictionary<string, Dictionary<string, string>> valueDict, ref Regex regexText, ref string docText)
        {
            string paragraph = "";
            regexText = new Regex("{{insertDevices}}");
            var i = 1;
            string comments = "Athugasemdir: ";
            foreach (var entry in valueDict)
            {
                var sub = entry.Value;
                var devices = from d in db.Devices
                              where d.gre == entry.Key
                              select d;
                var device = devices.ToArray()[0];
                paragraph += "Tæki " + i++ + ": " + getDeviceValueWhere(device, "Framleiðandi") + " " + getDeviceValueWhere(device, "Tegund gerð") + " (" + entry.Key + ")" + "<w:br />"
                    + "Innstilltur geislunartími var " + sub["v25"] + " msek, en mældur tími (meðaltal) var " + sub["v22"] + " msek ";

                var fravik_timi = double.Parse(sub["v23"], CultureInfo.InvariantCulture);
                if (fravik_timi < 20) paragraph += "(frávik undir 20%).";
                else paragraph += "(frávik yfir 20%). *";

                paragraph += "<w:br />";

                paragraph += "Geislaskammtur á mynd mældist ";
                var skammtur = double.Parse(sub["v20"], CultureInfo.InvariantCulture);
                var hamark = double.Parse(sub["v26"], CultureInfo.InvariantCulture);
                if (skammtur > hamark) paragraph += skammtur + " mGy. *";
                else paragraph += skammtur + " mGy.";

                paragraph += " Leyfilegt hámark er " + sub["v26"] +" mGy á mynd.<w:br />"
                    + "Geislaskammtur á tímaeiningu mældist " + sub["v21"] + " mGy/s.<w:br />"
                    + "Innstillt spenna var " + sub["v24"] + " kV, en mæld spenna (meðaltal) var " + sub["v18"] + " kV ";

                var fravik_spenna = double.Parse(sub["v19"], CultureInfo.InvariantCulture);
                if (fravik_spenna < 10) paragraph += "(frávik undir 10%).";
                else paragraph += "(frávik yfir 10%). *";

                paragraph += "<w:br />";

                if (sub["v1"]!="") paragraph += "Notaður er " + sub["v1"] + " millisekúndna myndatökutími fyrir bitewing fullorðinna.<w:br />";
                if (sub["v2"]!="") paragraph += "Notaður er " + sub["v2"] + " millisekúndna myndatökutími fyrir bitewing barna.<w:br />";
                if (fravik_timi > 20) comments += "Frávik geislunartíma meira en 20%. ";
                if (fravik_spenna > 10) comments += "Frávik geislaskammts á tímaeiningu meira en 10%.";
                paragraph += comments;

                paragraph += "<w:br /><w:br />";
            }
            docText = regexText.Replace(docText, paragraph);
            return docText;
        }

        private string InitializeWordObjects(UpFile wordFile, List<UpFile> outFilesToShow)
        {
            string copied = CopyDownFile(wordFile, null, null);
            var src = root + outFolder + globalDoc.Malsnumer + "\\" + copied;
            var up = new UpFile
            {
                Name = copied,
                Date = DateTime.Now,
                Purpose = "out",
                FileType = ".docx",
                Malsnumer = globalDoc.Malsnumer,
                Size = wordFile.Size,
            };
            db.UpFiles.Add(up);
            db.SaveChanges();
            outFilesToShow.Add(up);
            return src;
        }

        // Aðeins fyrir prófun
        private static string el0Test(ref string regex, Dictionary<string, string> el0, ref string docText)
        {
            Regex regexText;
            foreach (var entry in el0)
            {
                regex += "- ";

                regex += entry.Key + " " + entry.Value;
                regex += "<w:br />";
            }
            regex = regex.Replace("{{", ";  ").Replace("}}", ": ");
            regexText = new Regex("{{el0_test}}");
            docText = regexText.Replace(docText, regex);
            return docText;
        }

        // Word fall
        private string WriteDeviceList(ref string regex, ref string docText)
        {
            Regex regexText;
            foreach (var subDict in dictList.Skip(1))
            {
                regex += "- ";

                foreach (KeyValuePair<string, string> entry in subDict)
                {
                    regex += entry.Key + " " + entry.Value;
                }
                regex += "<w:br />";
            }
            regex = regex.Replace("{{", ";  ").Replace("}}", ": ");
            regexText = new Regex("{{beginDevices}}");
            docText = regexText.Replace(docText, regex);
            return docText;
        }

        // Word fall
        private static string WriteAuthorLicense(ref string docText, ref Regex regexText, Dictionary<string, string> el0)
        {
            foreach (KeyValuePair<string, string> entry in el0)
            {
                /* Það er eitthvað skrýtið í gangi með þessa Replace skipun.
                 * Stundum hætta SUMIR reitir að virka. Það virðist hrífa að
                 * skrifa reitina inn handvirkt. Hvernig sem það virkar...
                 *
                 * Það virkar að eyða alveg út því sem þú ert búinn að gera --- MEÐ bilum og ÖLLU --- og gera þetta upp á nýtt með því að SKRIFA það inn, ekki afrita og líma.
                 * Það virðist vera brothætt að breyta því sem fyrir er.
                 * "Ástæðan" er sú að í Word XML-inu virðist sem breytunum sé skipt í hluta þ.a. XML-ið heldur t.d. að 
                 * fyrir {{DocMalsnumer}} sé það {{D, M, alsnumer, }}. Þetta er mjög skrýtið en það virkar allavega að
                 * eyða öllu eins og minnst var á áðan.
                 */
                var key = entry.Key.ToString();
                if(key.Contains("Author_C373195379BC0424D226C396FEAFF32644CD02646089C1FA8D09B0695EE18034"))
                    key = key.Replace("Author_C373195379BC0424D226C396FEAFF32644CD02646089C1FA8D09B0695EE18034", "DocAuthor");
                regexText = new Regex(key);
                docText = regexText.Replace(docText, entry.Value);
            }
            return docText;
        }

        // Word fall
        private static string GetText(WordprocessingDocument wordDoc)
        {
            string docText;
            using (StreamReader sr = new StreamReader(wordDoc.MainDocumentPart.GetStream()))
            {
                docText = sr.ReadToEnd();
            }

            return docText;
        }

        // Word fall
        private static void WriteToDoc(WordprocessingDocument wordDoc, string docText)
        {
            using (StreamWriter sw = new StreamWriter(wordDoc.MainDocumentPart.GetStream(FileMode.Create)))
            {
                sw.Write(docText);
            }
        }


        // downFile er fyrirmynd, cL og device eru hlutir.
        // Fallið tekur gefin gildi og setur þau í viðeigandi Excel skjöl.
        // Fallið skilar fileName streng.
        private string Excel(UpFile downFile, Dictionary<string, string> dict, List<Device> devices, CompanyLicense cL = null, Device device = null, Dictionary<string, Dictionary<string, string>> valuesDict = null)
        {
            var greDict = new Dictionary<string, string>();
            var val = 0;

            string copied = CopyDownFile(downFile, cL, device);
            var i = 0;
            var cellString = "";

            // Using er gott fyrir for lykkjur
            var src = root + outFolder + globalDoc.Malsnumer + "\\" + copied;
            using (SpreadsheetDocument document = SpreadsheetDocument.Open(src, true))
            {
                IEnumerable<Row> allRows;
                bool begun = false;
                bool found = false;
                // Ómerkilegt fall. Bjó það til til að hafa þetta snyrtilegra.
                PrepareRows(document, out allRows, out begun);
                // Excel process - næsta stig fyrir neðan.
                ExcelProcess(dict, devices, cL, device, valuesDict, ref greDict, ref val, ref i, ref cellString, document, allRows, begun, ref found);
            }
            return copied;
        }

        public bool StringIsNumber(string str)
        {
            float value;
            return float.TryParse(str, out value);
        }

        // Stigin eru að ná enda hér. ProcessCompanyLicense fer að nálgast botninn.
        // StartExcel tekur við rows og devices, og smíðar Excel skjölin.
        // Mundu að ExcelProcess kallar á þetta. StartExcel er bara undirfall ExcelProcess.
        private void StartExcel(string rows, List<Device> devices, out CompanyLicense companyLicense, out UpFile devDownFile, out List<string> excelFileNames, out List<UpFile> outFilesToShow, out Dictionary<string, string> dict, out string fileName, out UpFile up, string values, out UpFile wordFile, out Dictionary<string, Dictionary<string,string>> valueDict)
        {
            var row_Split = rows.Split(new string[] { "," }, StringSplitOptions.None);
            UpFile downFile;
            InitializeFiles(devices, out companyLicense, out devDownFile, row_Split, out downFile, out wordFile);

            excelFileNames = new List<string>();
            outFilesToShow = new List<UpFile>();

            // Leyfishafi
            ProcessCompanyLicense(devices, companyLicense, excelFileNames, outFilesToShow, out dict, out fileName, out up, downFile);
            // End Leyfishafi

            // Tæki
            valueDict = createValueDict(values);
            ProcessDevices(devices, devDownFile, excelFileNames, outFilesToShow, ref dict, ref fileName, ref up, valueDict);
            // End Tæki
            //outFilesToShow.Add(wordFile);
        }

        // Ómerkilegt fall. Þarna sérðu samt mistökin sem ég gerði... 
        private void InitializeFiles(List<Device> devices, out CompanyLicense companyLicense, out UpFile devDownFile, string[] row_Split, out UpFile downFile, out UpFile wordFile)
        {
            companyLicense = GetCompanyLicense(row_Split, 2);
            downFile = GetUpFile(row_Split, 3);
            devDownFile = GetUpFile(row_Split, 4);
            wordFile = GetUpFile(row_Split, 5);
            var malsnumer = row_Split[row_Split.Length - 6].Split(':')[1].Replace("\"", "");
            var author = db.Authors.Find(ParseID(row_Split, 7));
            var doc = new Doc
            {
                Company = companyLicense,
                Devices = devices.ToList(),
                Malsnumer = malsnumer,
                Date = DateTime.Now,
                Author = author,
            };
            author.Docs.Add(doc);
            globalDoc = doc;
            db.Docs.Add(doc);
        }

        private void ProcessCompanyLicense(List<Device> devices, CompanyLicense companyLicense, List<string> excelFileNames, List<UpFile> upFilesToShow, out Dictionary<string, string> dict, out string fileName, out UpFile up, UpFile downFile)
        {
            dict = CreateDict(companyLicense);
            dict = ObjectToDict(dict, globalDoc);
            dict = ObjectToDict(dict, globalDoc.Author);
            // því ég nota þetta líka í Word ferlinu, ljótt að búa til tvær
            // þ.a. ég geymi þær í dictList
            dictList.Add(dict);
            fileName = Excel(downFile, dict, devices.ToList(), companyLicense);
            excelFileNames.Add(fileName);
            up = new UpFile()
            {
                Purpose = "out",
                Name = fileName,
                Malsnumer = globalDoc.Malsnumer,
                FileType = ".xlsx",
                Date = DateTime.Now,
            };
            db.UpFiles.Add(up);
            db.SaveChanges();
            upFilesToShow.Add(up);
        }

        // Almennara heldur en CreateDict. CreateDict er alltaf hvort eð er, ObjectToDict er sveigjanlegra
        private Dictionary<string,string> ObjectToDict(Dictionary<string, string> dict, object obj)
        {
            var properties = obj.GetType().GetProperties();
            var objString = obj.GetType().Name;
            foreach (var property in properties)
            {
                var val = property.GetValue(obj, null);
                // Gæti líka haft sér "if val is DateTime" til að klippa sólarhringstímann af val
                if (val != null && (val is string || val is DateTime))
                    dict["{{" + objString + property.Name + "}}"] = val.ToString();
            }
            
            return dict;
        }

        private void PrepareViewBag(List<Device> devices, CompanyLicense companyLicense, List<string> excelFileNames, UpFile wordFile)
        {
            // excelFileNames er gagnslaust.
            ViewBag.excelFileNames = excelFileNames;
            ViewBag.company = companyLicense;
            ViewBag.devices = devices;
            ViewBag.wordFile = wordFile;
        }

        private void ProcessSendString(string rows, out string values, out List<Device> devices)
        {
            var rowSplit = rows.Split(new string[] { ":[" }, StringSplitOptions.None);
            values = "";
            devices = new List<Device>();
            foreach (var item in rowSplit.Skip(1))
            {
                int value;
                var itemSplit = item.Split('\"')[1];
                var parse = int.TryParse(itemSplit, out value);
                if (!parse)
                {
                    values = item.Split(']')[0];
                    continue;
                }
                devices.Add(db.Devices.Find(value));
            }
        }

        private static int ParseID(string[] row_Split, int back)
        {
            return Int32.Parse(row_Split[row_Split.Length - back].Split(':')[1].Replace("\"", ""));
        }

        private CompanyLicense GetCompanyLicense(string[] row_Split, int back)
        {
            return db.CompanyLicenses.Find(ParseID(row_Split, back));
        }

        private UpFile GetUpFile(string[] row_Split, int back)
        {
            return db.UpFiles.Find(ParseID(row_Split, back));
        }


        private void ProcessDevices(List<Device> devices, UpFile devDownFile, List<string> excelFileNames, List<UpFile> upFilesToShow, ref Dictionary<string, string> dict, ref string fileName, ref UpFile up, Dictionary<string, Dictionary<string, string>> radioDict)
        {
            foreach (var device in devices)
            {
                dict = CreateDict(null, device);
                dict = ObjectToDict(dict, globalDoc);
                dictList.Add(dict);
                fileName = Excel(devDownFile, dict, null, null, device, radioDict);
                excelFileNames.Add(fileName);

                up = new UpFile
                {
                    Purpose = "out",
                    Name = fileName,
                    Malsnumer = globalDoc.Malsnumer,
                    FileType = ".xlsx",
                    Date = DateTime.Now
                };

                upFilesToShow.Add(up);
                db.UpFiles.Add(up);
            }
            db.UpFiles.Add(up);
        }

        private static Dictionary<string, Dictionary<string, string>> createValueDict(string values)
        {
            var valueDict = new Dictionary<string, Dictionary<string, string>>();
            var valueNumberDict = new Dictionary<string, string>();
            string[] summarySplit;
            string[] summarySplitLatter;
            string valueNumber;
            string GRE;
            string choiceNumber;
            bool success;
            bool successV;
            int val;
            int valV;
            foreach (var summary in values.Split(','))
            {
                summarySplit = summary.Split('_');
                summarySplitLatter = summarySplit[1].Split('=');
                SetValueNumber(summarySplit, out valueNumber, out success, out successV, out val, out valV);
                GRE = summarySplitLatter[0];
                choiceNumber = SetChoiceNumber(summarySplitLatter);
                // ProcessValueDict er: valueDict[GRE][valueNumber] = choiceNumber
                ProcessValueDict(valueDict, valueNumber, GRE, choiceNumber);
            }
            return valueDict;
        }

        private static void SetValueNumber(string[] summarySplit, out string valueNumber, out bool success, out bool successV, out int val, out int valV)
        {
            success = int.TryParse(summarySplit[0].Replace("r", "").Replace("\"", ""), out val);
            successV = int.TryParse(summarySplit[0].Replace("v", "").Replace("\"", ""), out valV);
            if (success)
            {
                valueNumber = "r" + val.ToString();
                success = false;
            }
            else if (successV)
            {
                valueNumber = "v" + summarySplit[0].Replace("v", "").Replace("\"", "");
                successV = false;
            }
            else
            {
                valueNumber = "c" + summarySplit[0].Replace("c", "").Replace("\"", "");
            }
        }

        private static void ProcessValueDict(Dictionary<string, Dictionary<string, string>> valueDict, string valueNumber, string GRE, string choiceNumber)
        {
            if (valueDict.ContainsKey(GRE))
            {
                valueDict[GRE][valueNumber] = choiceNumber;
            }
            else
            {
                valueDict[GRE] = new Dictionary<string, string>();
                valueDict[GRE][valueNumber] = choiceNumber;
            }
        }

        private static string SetChoiceNumber(string[] summarySplitLatter)
        {
            string choiceNumber;
            try
            {
                choiceNumber = summarySplitLatter[1].Replace("\"", "");
            }
            catch (System.FormatException)
            {
                choiceNumber = summarySplitLatter[1].Replace("\"]", "");
            }
            return choiceNumber;
        }

        private void ExcelProcess(Dictionary<string, string> dict, List<Device> devices, CompanyLicense cL, Device device, Dictionary<string, Dictionary<string, string>> valuesDict, ref Dictionary<string, string> greDict, ref int val, ref int i, ref string cellString, SpreadsheetDocument document, IEnumerable<Row> allRows, bool begun, ref bool found)
        {
            foreach (Row currentRow in allRows)
            {
                IEnumerable<Cell> allCells = currentRow.Descendants<Cell>();
                foreach (Cell currentCell in allCells)
                {
                    CellValue currentCellValue = currentCell.GetFirstChild<CellValue>();
                    string data = PrepareCellData(document, currentCell, currentCellValue);

                    if (data != null)
                    {
                        if (dict.ContainsKey(data))
                        {
                            // Void. Þar sem dict heldur upp á aðalgögnin (þau sem koma úr gagnagrunninum)
                            // þá heitir þetta EditPrimaryCells.
                            EditPrimaryCells(dict, cellString, currentCell, currentCellValue, data);
                        }

                        else if (cL != null && data.Contains("{-{") && data.Contains("}-}"))
                        {
                            // Primary Device Cells þýðir tækjaklefarnir í Leyfishafafyrirmyndinni. (brúna taflan fyrir miðju)
                            begun = true;
                            EditPrimaryDeviceCells(devices, i, ref cellString, currentCell, currentCellValue, data);
                        }
                        else if (valuesDict != null && data.Contains("{+{") && data.Contains("}+}"))
                        {
                            ProcessFields(device, valuesDict, ref greDict, ref val, ref found, currentCell, currentCellValue, data);
                        }
                    }
                }
                // Ef byrjað er að skrifa tækin inn í Main skjalið, þá byrja að
                // hækka um i því i heldur utan um númer hvaða tæki er verið að setja inn. 
                // Sjá begun = true og kóðann kringum það.
                if (begun)
                    i++;
            }
        }

        private void ProcessFields(Device device, Dictionary<string, Dictionary<string, string>> valuesDict, ref Dictionary<string, string> greDict, ref int val, ref bool found, Cell currentCell, CellValue currentCellValue, string data)
        {
            string cellString = data.Replace("{+{", "").Replace("}+}", "");
            if (valuesDict.ContainsKey(device.gre))
            {
                greDict = valuesDict[device.gre];
            } 
            else
            {
                found = false;
            }
            if (cellString.Split('r').Length > 1)
            {
                found = EditRadios(greDict, cellString, found, currentCell, currentCellValue);
            }
            else if (cellString.Split('v').Length > 1)
            {
                float valFloat=0;
                found = EditValues(greDict, ref valFloat, cellString, ref found, currentCell, currentCellValue);
            }
            else if (cellString.Split('c').Length > 1)
            {
                found = EditCheckboxes(greDict, cellString, found, currentCell, currentCellValue);
            }
            if(!found)
            {
                currentCellValue.Text = "";
            }
        }

        private bool EditRadios(Dictionary<string, string> greDict, string cellString, bool found, Cell currentCell, CellValue currentCellValue)
        {
            found = false;
            if (greDict.ContainsKey(CorrectKeyFormat(cellString, 'r')))
            {
                if (greDict[CorrectKeyFormat(cellString,'r')] == cellString.Split('r')[0])
                {
                    CorrectType(currentCell, cellString);
                    currentCellValue.Text = "X";
                    found = true;
                }
            }
            return found;
        }

        private bool EditValues(Dictionary<string, string> greDict, ref float val, string cellString, ref bool found, Cell currentCell, CellValue currentCellValue)
        {
            found = false;
            var correctKey = CorrectKeyFormat(cellString, 'v');

            if (greDict.ContainsKey(correctKey))
            {
                if (float.TryParse(greDict[correctKey], out val))
                {
                    CorrectType(currentCell, cellString);
                    if (greDict[correctKey].Length > 6)
                        currentCellValue.Text = greDict[correctKey].Substring(0, 7);
                    else
                        currentCellValue.Text = greDict[correctKey];
                    
                    found = true;
                }
            }
            return found;
        }

        private bool EditCheckboxes(Dictionary<string, string> greDict, string cellString, bool found, Cell currentCell, CellValue currentCellValue)
        {
            found = false;
            if (greDict.ContainsKey(CorrectKeyFormat(cellString, 'c')))
            {
                if (greDict[CorrectKeyFormat(cellString, 'c')] == "on")
                {
                    CorrectType(currentCell, cellString);
                    currentCellValue.Text = "X";
                    found = true;
                }
            }

            return found;
        }

        private void EditPrimaryDeviceCells(List<Device> devices, int i, ref string cellString, Cell currentCell, CellValue currentCellValue, string data)
        {
            // Ef búið er að setja öll tæki inn í meginskjalið
            // þá skal eyða þeim klefum sem eiga við.
            if (i >= devices.Count)
            {
                currentCellValue.Text = "";
            }
            else
            {
                cellString = data.Replace("{-{", "").Replace("}-}", "");
                CorrectType(currentCell, cellString);
                currentCellValue.Text = getDeviceValueWhere(devices[i], cellString);
            }
        }

        private static string CorrectKeyFormat(string cellString, char ch)
        {
            return ch + cellString.Split(ch)[1];
        }

        private void EditPrimaryCells(Dictionary<string, string> dict, string cellString, Cell currentCell, CellValue currentCellValue, string data)
        {
            CorrectType(currentCell, cellString);
            currentCellValue.Text = dict[data];
        }

        private static void PrepareRows(SpreadsheetDocument document, out IEnumerable<Row> allRows, out bool begun)
        {
            Sheet sheet = document.WorkbookPart.Workbook.Descendants<Sheet>().First<Sheet>();
            Worksheet worksheet = ((WorksheetPart)document.WorkbookPart.GetPartById(sheet.Id)).Worksheet;
            allRows = worksheet.GetFirstChild<SheetData>().Descendants<Row>();
            begun = false;
        }

        private static string PrepareCellData(SpreadsheetDocument document, Cell currentCell, CellValue currentCellValue)
        {
            string data = null;
            if (currentCell.DataType != null)
            {
                if (currentCell.DataType == CellValues.SharedString) // cell has a cell value that is a string, thus, stored else where
                {
                    if (!(currentCellValue == null || currentCellValue.Text == "")) data = document.WorkbookPart.GetPartsOfType<SharedStringTablePart>().FirstOrDefault().SharedStringTable.ElementAt(int.Parse(currentCellValue.Text)).InnerText;
                }
            }
            else if (currentCellValue != null)
            {
                data = currentCellValue.Text;
            }

            return data;
        }

        private string CopyDownFile(UpFile downFile, CompanyLicense cL, Device device)
        {
            var split = downFile.Name.Split('.');
            var copied = "";
            var initFile = root + uploaded + downFile.Name;
            var dest = root + outFolder + globalDoc.Malsnumer + "\\";
            Directory.CreateDirectory(dest);

            if (cL != null)
            {
                copied = cL.Name + "." + split[1];
                System.IO.File.Copy(initFile, dest + copied, true);
            }
            else if (device != null)
            {
                copied = device.Identify() + "." + split[1];
                System.IO.File.Copy(initFile, dest + copied, true);
            }
            else
            {
                copied = downFile.Name;
                System.IO.File.Copy(initFile, dest + copied, true);
            }
            return copied;
        }

        private Dictionary<string,string> CreateDict(CompanyLicense companyLicense=null, Device device=null)
        {
            var dict = new Dictionary<string, string>();
            if (companyLicense!=null)
            {
                foreach (var property in companyLicense.GetType().GetProperties())
                {
                    try
                    {
                        // Holy shit hvað þetta er cool
                        // motherfucker
                        var propVal = property.GetValue(companyLicense, null).ToString();
                        var propEval = "{{" + property.Name + "}}";
                        if (propVal == "True")
                        {
                            dict[propEval] = "X";
                        }
                        else if (propVal == "False")
                        {
                            dict[propEval] = "";
                        }
                        else
                        {
                            dict[propEval] = propVal;
                        }
                    }
                    catch (NullReferenceException)
                    {
                        // Ef notandinn fyllti ekki í alla reitina kemur NRE. 
                        // Nú sér Excel um að setja í tóma reiti þar sem ekkert var valið
                        // vegna tóma strengsins hér að neðan.
                        dict["{{" + property.Name + "}}"] = "";
                    }
                }
            }
            else
            {
                var i = 0;
                var columns = device.UpFile.Columns.Split(';');
                foreach (var value in device.values.Split(';'))
                {
                    dict["{{" + columns[i++] + "}}"] = value;
                }
            }
            
            return dict;
        }
        [Authorize]
        public ActionResult DL(string name, bool blueprint= false, string dir="")
        {
            if (dir != "") dir += "\\";
            var path = root + outFolder + dir + name;
            var mimeType = "vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8";
            if(!blueprint) return File(path, mimeType, Server.UrlEncode(name));
            else return File(root+uploaded+name, mimeType, Server.UrlEncode(name));
        }

        // Sækir þann dálk í tækinu device sem heyrir undir dálkinn replace í UpFile.Columns.
        private string getDeviceValueWhere(Device device, string replace)
        {
            var columns = device.UpFile;
            var index = Array.IndexOf(columns.Columns.Split(';'), replace);
            return device.values.Split(';')[index];
        }

        private void CorrectType(Cell cell, string cellString)
        {
            if (StringIsNumber(cellString)) 
                cell.DataType = new EnumValue<CellValues>(CellValues.Number);
            else 
                cell.DataType = new EnumValue<CellValues>(CellValues.String);
        }

        [Authorize]
        public ActionResult Crosses(int id)
        {
            return PartialView("_Crosses"+"_"+id);
        }

        // GET: Docs/Details/5
        [Authorize]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Doc doc = db.Docs.Find(id);
            if (doc == null)
            {
                return HttpNotFound();
            }
            return View(doc);
        }

        // GET: Docs/Create
        [Authorize]
        public ActionResult Create()
        {
            ViewBag.devices = db.Devices.ToList();
            return View();
        }

        // POST: Docs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Create([Bind(Include = "ID,Title,Company,Devices")] Doc doc)
        {
            return RedirectToAction("Index");
        }

        public ActionResult CreateCompanyLicense()
        {
            return View();
        }

        [Authorize]
        public ActionResult CL()
        {
            var m = from c in db.CompanyLicenses
                    select c;
            return View(m);
        }

        public ActionResult CreateCL()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult CreateCL(CompanyLicense CompanyLicense)
        {
            if (ModelState.IsValid)
            {
                db.CompanyLicenses.Add(CompanyLicense);
                db.SaveChanges();
                return RedirectToAction("CL");
            }
            return View();
        }
        // GET: Docs/Details/5
        [Authorize]
        public ActionResult DetailsCL(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CompanyLicense CompanyLicense = db.CompanyLicenses.Find(id);
            if (CompanyLicense == null)
            {
                return HttpNotFound();
            }
            return View(CompanyLicense);
        }

        // GET: Docs/Edit/5
        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Doc doc = db.Docs.Find(id);
            if (doc == null)
            {
                return HttpNotFound();
            }
            return View(doc);
        }

        // POST: Docs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Edit([Bind(Include = "ID,Title,Company,Text1,Date,Author,gre")] Doc doc)
        {
            if (ModelState.IsValid)
            {
                db.Entry(doc).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(doc);
        }

        // GET: Docs/Delete/5
        [Authorize]
        public ActionResult DeleteCL(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var cl = db.CompanyLicenses.Find(id);
            if (cl == null)
            {
                return HttpNotFound();
            }
            return View(cl);
        }

        // POST: Docs/Delete/5
        [HttpPost, ActionName("DeleteCL")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult DeleteCLConfirmed(int id)
        {
            var cl = db.CompanyLicenses.Find(id);
            db.CompanyLicenses.Remove(cl);
            db.SaveChanges();
            return RedirectToAction("CL");
        }

        // GET: Docs/Delete/5
        [Authorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Doc doc = db.Docs.Find(id);
            if (doc == null)
            {
                return HttpNotFound();
            }
            return View(doc);
        }

        // POST: Docs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult DeleteConfirmed(int id)
        {
            Doc doc = db.Docs.Find(id);
            Directory.Delete(root + outFolder + doc.Malsnumer, true);
            db.Docs.Remove(doc);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
