﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using steer.Models;
using LinqToExcel;

namespace steer.Controllers
{
    public class FilesController : Controller
    {
        private UpFile.ApplicationDbContext db = new UpFile.ApplicationDbContext();

        // GET: Files
        [Authorize]
        public ActionResult Index()
        {
            var fileList = from f in db.UpFiles
                           where f.Purpose != "out"
                           select f;
            return View(fileList);
        }


        // GET: Files/Details/5
        [Authorize]
        public ActionResult Details(int? id)
        {
            ViewBag.companies = db.CompanyLicenses.ToList();
            var companyDownFiles = from c in db.UpFiles
                                where c.Purpose == "company"
                                select c;
            ViewBag.companyDownFiles = companyDownFiles;
            var deviceDownFiles = from d in db.UpFiles
                                  where d.Purpose == "device"
                                  select d;
            ViewBag.deviceDownFiles = deviceDownFiles;
            var wordDownFiles = from w in db.UpFiles
                                where w.Purpose == "word"
                                select w;
            ViewBag.wordDownFiles = wordDownFiles;
            var authors = from a in db.Authors
                          select a;
            ViewBag.authors = authors;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UpFile upFile = db.UpFiles.Find(id);
            if (upFile == null)
            {
                return HttpNotFound();
            }

            return View(upFile);
        }

        // GET: Files/Create
        [Authorize]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Upload/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Name,FileType,Size")] UpFile upFile)
        {
            if (ModelState.IsValid)
            {
                db.UpFiles.Add(upFile);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(upFile);
        }

        // GET: Files/Edit/5
        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UpFile upFile = db.UpFiles.Find(id);
            if (upFile == null)
            {
                return HttpNotFound();
            }
            return View(upFile);
        }

        // POST: Files/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Edit([Bind(Include = "ID,Name,FileType,Size")] UpFile upFile)
        {
            if (ModelState.IsValid)
            {
                db.Entry(upFile).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(upFile);
        }
        [Authorize]
        public ActionResult Generated()
        {
            var outFiles = from f in db.UpFiles
                           where f.Purpose == "out"
                           select f;
            return View(outFiles);
        }
        [Authorize]
        public ActionResult CL()
        {
            var CLFiles = from c in db.UpFiles
                          where c.Purpose == "company"
                          select c;
            return View(CLFiles);
        }
        [Authorize]
        public ActionResult Reports()
        {
            var reports = from r in db.UpFiles
                          where r.Purpose == "word"
                          select r;
            return View(reports);
        }
        [Authorize]
        public ActionResult Dev()
        {
            var devFiles = from d in db.UpFiles
                           where d.Purpose == "device"
                           select d;
            return View(devFiles);
        }

        // GET: File/Delete/5
        [Authorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UpFile upFile = db.UpFiles.Find(id);
            if (upFile == null)
            {
                return HttpNotFound();
            }
            return View(upFile);
        }

        // POST: File/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult DeleteConfirmed(int id)
        {
            UpFile upFile = db.UpFiles.Find(id);
            db.UpFiles.Remove(upFile);
            string path = AppDomain.CurrentDomain.BaseDirectory + "Uploaded\\" + upFile.Name;
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);
            }
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
