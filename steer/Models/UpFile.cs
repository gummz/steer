﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace steer.Models
{
    public class UpFile
    {
        [Display(Name="Auðkenni")]
        public int ID { get; set; }

        [Required]
        [Display(Name="Nafn")]
        public string Name { get; set; }

        [Display(Name="Málsnúmer")]
        public string Malsnumer { get; set; }

        [Required]
        [Display(Name = "Hlaðið þann")]
        public DateTime Date { get; set; }

        [Required]
        [Display(Name="Tegund")]
        public string FileType { get; set; }

        [Display(Name = "Stærð (kB)")]
        public int Size { get; set; }
        public string Columns { get; set; }
        public UpFile()
        {
            Devices = new List<Device>();
        }
        [Display(Name="Tæki")]
        public virtual ICollection<Device> Devices { get; set; }

        [Display(Name="Tilgangur")]
        public string Purpose { get; set; }

        public class ApplicationDbContext : DbContext
        {
            public DbSet<UpFile> UpFiles { get; set; }
            public DbSet<Device> Devices { get; set; }
            public DbSet<Doc> Docs { get; set; }
            public DbSet<CompanyLicense> CompanyLicenses { get; set; }
            public DbSet<Author> Authors { get; set; }
        }
    }

    public class Device
    {
        [Display(Name ="Auðkenni")]
        public int ID { get; set; }
        public string values { get; set; }

        [ForeignKey("UpFile")]
        public int UpFileID { get; set; }
        public string gre { get; set; }

        public virtual UpFile UpFile { get; set; }
        public virtual ICollection<Doc> Docs { get; set; }

        public string Identify()
        {
            return values.Split(';')[(Array.IndexOf(this.UpFile.Columns.Split(';'), "Tegund gerð"))] + " " + this.gre;
        }
    }
}