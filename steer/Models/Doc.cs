﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace steer.Models
{

    public class Doc
    {
        [Display(Name = "Auðkenni")]
        public int ID { get; set; }

        [Display(Name = "Leyfishafi")]
        public virtual CompanyLicense Company { get; set; }

        [ScriptIgnore(ApplyToOverrides = true)]
        [Display(Name = "Tæki")]
        public virtual ICollection<Device> Devices { get; set; }

        public string Malsnumer { get; set; }

        [Display(Name = "Skráarheiti")]
        public string FileNames { get; set; }

        [Required]
        [Display(Name = "Skrifað þann")]
        public DateTime Date { get; set; }

        [Display(Name = "Höfundur")]
        public virtual Author Author { get; set; }

        public Doc()
        {
            Devices = new List<Device>();
        }
    }

    public class CompanyLicense
    {
        public int ID { get; set; }

        [Display(Name = "Reglubundið eftirlit GR")]
        public bool Reglubundid { get; set; }
        [Display(Name = "Úttek/Ný skoðun á aðstöðu/Tæki/Notkun")]
        public bool NySkodun { get; set; }
        [Display(Name = "Leyfishafi (heiti/Nafn)")]
        public string Name { get; set; }
        [Display(Name = "Kennitala")]
        public string Kt { get; set; }
        [Display(Name = "Símanúmer")]
        public string Phone { get; set; }
        [Display(Name = "Lögheimili")]
        public string Address { get; set; }
        [Display(Name = "Póstnúmer")]
        public string Postal { get; set; }
        [Display(Name = "Staður")]
        public string Area { get; set; }
        [Display(Name = "Notkunarstaður (ef annar en lögheimili")]
        public string PlaceOfUse { get; set; }
        [Display(Name = "Netfang")]
        public string Email { get; set; }

        [Display(Name = "Nafn ábyrgðarmanns")]
        public string AssocName { get; set; }
        [Display(Name = "Kennitala ábyrgðarmanns")]
        public string AssocKt { get; set; }
        [Display(Name = "Netfang ábyrgðarmanns")]
        public string AssocEd { get; set; }
        [Display(Name = "Menntun ábyrgðarmanns")]
        public string AssocEmail { get; set; }
        [Display(Name = "Námskeið GR?")]
        public bool NamskeidGR { get; set; }
    }
    public class Author
    {
        public int ID { get; set; }
        [Display(Name = "Nafn")]
        public string Name { get; set; }
        [Display(Name="Upphafsstafir")]
        public string Initials { get; set; }
        [Display(Name="Sérfræðikunnátta")]
        public string Specialty { get; set; }
        public virtual ICollection<Doc> Docs { get; set; }
    }
}