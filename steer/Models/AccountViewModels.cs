﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace steer.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Netfang")]
        public string Email { get; set; }
        public string FullName { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Kóði")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Muna þennan vafra?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Netfang")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Netfang")]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Lykilorð")]
        public string Password { get; set; }

        [Display(Name = "Muna eftir mér")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Netfang")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "{0} verður að vera a.m.k. {2} stafa langt.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Lykilorð (aftur)")]
        [Compare("Password", ErrorMessage = "Lykilorðin stangast á.")]
        public string ConfirmPassword { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Netfang")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "{0} verður að vera a.m.k. {2} stafa langt.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Lykilorð")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Lykilorð (aftur)")]
        [Compare("Password", ErrorMessage = "Lykilorðin stangast á.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Netfang")]
        public string Email { get; set; }
    }
}
