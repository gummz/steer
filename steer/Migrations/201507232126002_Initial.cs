namespace steer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CompanyLicenses",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Reglubundid = c.Boolean(nullable: false),
                        NySkodun = c.Boolean(nullable: false),
                        Name = c.String(),
                        Kt = c.String(),
                        Phone = c.String(),
                        Address = c.String(),
                        Postal = c.String(),
                        Area = c.String(),
                        PlaceOfUse = c.String(),
                        Email = c.String(),
                        AssocName = c.String(),
                        AssocKt = c.String(),
                        AssocEd = c.String(),
                        AssocEmail = c.String(),
                        NamskeidGR = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Devices",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        values = c.String(),
                        UpFileID = c.Int(nullable: false),
                        gre = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UpFiles", t => t.UpFileID, cascadeDelete: true)
                .Index(t => t.UpFileID);
            
            CreateTable(
                "dbo.Docs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        Author = c.String(),
                        Company_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.CompanyLicenses", t => t.Company_ID)
                .Index(t => t.Company_ID);
            
            CreateTable(
                "dbo.UpFiles",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Date = c.DateTime(nullable: false),
                        FileType = c.String(nullable: false),
                        Size = c.Int(nullable: false),
                        Columns = c.String(),
                        Purpose = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.DocDevices",
                c => new
                    {
                        Doc_ID = c.Int(nullable: false),
                        Device_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Doc_ID, t.Device_ID })
                .ForeignKey("dbo.Docs", t => t.Doc_ID, cascadeDelete: true)
                .ForeignKey("dbo.Devices", t => t.Device_ID, cascadeDelete: true)
                .Index(t => t.Doc_ID)
                .Index(t => t.Device_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Devices", "UpFileID", "dbo.UpFiles");
            DropForeignKey("dbo.DocDevices", "Device_ID", "dbo.Devices");
            DropForeignKey("dbo.DocDevices", "Doc_ID", "dbo.Docs");
            DropForeignKey("dbo.Docs", "Company_ID", "dbo.CompanyLicenses");
            DropIndex("dbo.DocDevices", new[] { "Device_ID" });
            DropIndex("dbo.DocDevices", new[] { "Doc_ID" });
            DropIndex("dbo.Docs", new[] { "Company_ID" });
            DropIndex("dbo.Devices", new[] { "UpFileID" });
            DropTable("dbo.DocDevices");
            DropTable("dbo.UpFiles");
            DropTable("dbo.Docs");
            DropTable("dbo.Devices");
            DropTable("dbo.CompanyLicenses");
        }
    }
}
