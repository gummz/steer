﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace steer
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "Files",
                url: "{controller}/{action}",
                defaults: new { controller = "Files", action = "Index" }
            );
            routes.MapRoute(
                name: "Docs",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Docs", action = "Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "Authors",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Authors", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
