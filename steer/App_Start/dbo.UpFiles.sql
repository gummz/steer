﻿CREATE TABLE [dbo].[UpFiles] (
    [ID]       INT            IDENTITY (1, 1) NOT NULL,
    [Name]     NVARCHAR (MAX) NOT NULL,
    [FileType] NVARCHAR (MAX) NULL,
    [Size]     INT            NOT NULL,
    CONSTRAINT [PK_dbo.UpFiles] PRIMARY KEY CLUSTERED ([ID] ASC)
);

